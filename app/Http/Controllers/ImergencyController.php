<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\facades\DB;

class ImergencyController extends Controller
{
    //
    public function imergency($worker_id,$workspace_id)
    {
        # code...
        $worker=DB::table('workers')
        ->where("worker_id",$worker_id)
        ->get();

        $workspace=DB::table('workspaces')
        ->get();

        return view('dashboard.imergency',['worker'=>$worker],['workspaces'=>$workspace]);

    }
    

    public function callAmbulance(Request $req)
    {
        $addcall = DB::table("callingambulance")
        ->insert([
            "workspace_id"=>$req->input("workspace_id"),
            "called_workspace"=>$req->input("called_workspace"),
            "message"=>$req->input("message")
        ]);

        return back();
    }
}
