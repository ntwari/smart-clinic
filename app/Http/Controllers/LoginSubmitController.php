<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\facades\DB;

class LoginSubmitController extends Controller
{
    //
    public function login(Request $req)
    {
        # code...
        $worker=DB::table('workers')
        ->where('password',$req->input('workspace-pass'))
        ->get();

        $todayDate=date('Y-m-d');

        $todayHistory=DB::table("clinichistory")
        ->where("created_at",date('Y-m-d'))
        ->get();
        // date('Y-m-d')
        $i=1;

        return view("dashboard.dashboard",compact("worker","todayHistory","i","todayDate"));
    }
}
