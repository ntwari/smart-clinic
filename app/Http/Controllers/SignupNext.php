<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\facades\DB;

class SignupNext extends Controller
{
    //
    public function SignupNext(Request $req)
    {
        $editUser=DB::table('workspaces')
        ->where("work_name",$req->input('workspacename'))
        ->update([
            "province"=>$req->input("province"),
            "district"=>$req->input("district"),
            "sector"=>$req->input("sector"),
            "village"=>$req->input("village"),
            "hospital_type"=>$req->input("hospital_type"),
            "num_workers"=>$req->input("worker_num"),
            "functionality"=>$req->input("functionality")
        ]);
        return view('login')
        ->with('success','You have done with creating the workspace ');


    }
}
