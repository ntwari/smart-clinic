<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\facades\DB;

class ClinicHistoryController extends Controller
{
    //
    public function openHistory($clinic_date,$workspace_id,$worker_id)
    {
        $selectedHistory = DB::table('clinichistory')
        ->where('created_at',$clinic_date)
        ->get();

        $worker=DB::table('workers')
        ->where("worker_id",$worker_id)
        ->get();

        $i=1;

        return view('history.openHistory',compact('worker','selectedHistory','i'));
    }
    public function generatepdf($clinic_date,$workspace_id)
    {
        return "creating the pdf of records  ... ";
    }
    public function addNewRecord(Request $req,$clinic_date,$workspace_id)
    {
        if($req->input("first")=="true"){
            $history=DB::table("history")
            ->insert([
                "workspace_id"=>$workspace_id,
                "hist_date"=>date('Y-m-d'),
                "number_of_records"=>1
            ]);
        }

        $patient=DB::table('clinichistory')
        ->insert([
            "workspace_id"=>$workspace_id,
            "first_name"=>$req->input("firstName"),
            "second_name"=>$req->input("secondName"),
            "district"=>$req->input("district"),
            "sector"=>$req->input("sector"),
            "village"=>$req->input("village"),
            "cell"=>$req->input("cell"),
            "symptoms"=>$req->input("symptoms"),
            "medicine"=>$req->input("medicine"),
            "payed"=>$req->input("payed"),
            "created_at"=>$req->input("date")
        ]);
        return back();
    }

    public function deleteRecord($record_id)
    {
        # code...
        $deleteRecord=DB::table('clinichistory')
        ->where('id',$record_id)
        ->delete();

        return back()->with("success","The record deleted now ! ");
    }

    public function editRecord($record_id)
    {
        # code...
        $record = DB::table("clinichistory")
        ->where('id',$record_id)
        ->get();
        
        $worker=DB::table('workers')
        ->where("worker_id",1)
        ->get();

        return view('history.editRecord',compact('worker','record'));
    }

    public function editingRecord(Request $req)
    {
        # code...
        $patient=DB::table('clinichistory')
        ->where("id",$req->input("record_id"))
        ->update([
            "first_name"=>$req->input("firstName"),
            "second_name"=>$req->input("secondName"),
            "district"=>$req->input("district"),
            "sector"=>$req->input("sector"),
            "village"=>$req->input("village"),
            "cell"=>$req->input("cell"),
            "symptoms"=>$req->input("symptoms"),
            "medicine"=>$req->input("medicine"),
            "payed"=>$req->input("payed")
        ]);


        return back();
    }

}