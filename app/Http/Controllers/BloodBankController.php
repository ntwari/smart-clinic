<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\facades\DB;

class BloodBankController extends Controller
{
    //
    public function hospitalStore($worker_id,$workspace_id)
    {
        $worker=DB::table('workers')
        ->where("worker_id",$worker_id)
        ->get();

        $storedBlood = DB::table('bloodBank')
        ->where('status',$workspace_id)
        ->paginate(3);

        return view('bloodBank.bloodBank',['bloodBank'=>$storedBlood],['worker'=>$worker]);
    }
    public function lifebank()
    {
        $worker=DB::table('workers')
        ->where("worker_id",1)
        ->get();

        $storedBlood = DB::table('bloodBank')
        ->where('status',1)
        ->paginate(3);

        return view('bloodBank.lifebank',['bloodBank'=>$storedBlood],['worker'=>$worker]);
    }
}
