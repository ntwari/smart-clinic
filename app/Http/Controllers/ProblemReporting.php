<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\facades\DB;

class ProblemReporting extends Controller
{
    //
    public function report($worker_id,$workspace_id)
    {
        # code...
        $worker=DB::table('workers')
        ->where("worker_id",$worker_id)
        ->get();

        
        $head=DB::table('workers')
        ->where("department","head of hospital")
        ->get();

        $manager=DB::table('workers')
        ->where("department","manager")
        ->get();

        $accountant=DB::table('workers')
        ->where("department","accountant")
        ->get();

        return view('dashboard.problemReporting',compact('worker','head','manager','accountant'));
    }

    public function reportToHead(Request $req)
    {
        # code...
        $report=DB::table("problemReport")
        ->insert([
            "reporter_id"=>$req->input("reporter"),
            "cheif_id"=>$req->input("hoh_id"),
            "report"=>$req->input("report"),
        ]);
        return back();
    }
}
