<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\facades\DB;

class DashboardController extends Controller
{
    //
    public function index($worker_id,$workspace_id)
    {
        $worker=DB::table('workers')
        ->where("worker_id",$worker_id)
        ->get();

        $todayDate=date('Y-m-d');

        $todayHistory=DB::table("clinichistory")
        ->where("created_at",date('Y-m-d'))
        ->get();
        // date('Y-m-d')
        $i=1;

        return view("dashboard.dashboard",compact("worker","todayHistory","i","todayDate"));

    }
    public function clinicHistory($worker_id,$workspace_id)
    {
        $worker=DB::table('workers')
        ->where("worker_id",$worker_id)
        ->get();

        $latesthistory=DB::table('history')
        ->where("workspace_id",$workspace_id)
        ->paginate(1);
        
        $i=0;

        $allhistory =DB::table('history')
        ->where("workspace_id",$workspace_id)
        ->get();

        $counter = 0;

        return view("dashboard.clinicHistory",compact('worker','latesthistory','allhistory','counter'));
    }
}