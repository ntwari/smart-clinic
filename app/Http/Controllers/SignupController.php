<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\facades\DB;

class SignupController extends Controller
{
    //
    public function SignupFunction(Request $req)
    {
        $user=DB::table("workspaces")
        ->insert([
            "work_name"=>$req->input("workspacename"),
            "ver_email"=>$req->input("workspaceemail"),
            "province"=>"",
            "district"=>"",
            "sector"=>"",
            "village"=>"",
            "hospital_type"=>"",
            "num_workers"=>0,
            "functionality"=>""

        ]);

        $workspace=DB::table('workspaces')
        ->where("work_name",$req->input("workspacename"))
        ->paginate(1);

        return view("signup_next",compact('workspace'));
    }
}
