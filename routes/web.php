<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/login',function(){
    return view('login');
});
Route::get('/home',function(){
    return view('welcome');
});
Route::get('/signup',function(){
    return view('signup');
});
Route::post('signupSend','SignupController@SignupFunction');
Route::get('/signup_next',function(){
    return view('signup_next');
});
Route::get('/patientRecords/{worker_id}/{workspace_id}','DashboardController@index');
// Route::get('/dashboard','DashboardController@index')->name('dashboard');
Route::get('/clinicHistory/{worker_id}/{workspace_id}','DashboardController@clinicHistory');
Route::post('signup_next','SignupNext@SignupNext');

//clinic routers
Route::get('/openHistory/{clinic_date}/{workspace_id}/{worker_id}','ClinicHistoryController@openHistory');
Route::get('/generatepdf/{clinic_date}/{workspace_id}','ClinicHistoryController@generatepdf');
Route::post('addNewRecord/{clinic_date}/{workspace_id}','ClinicHistoryController@addNewRecord');
Route::get('/deleteRecord/{record_id}','ClinicHistoryController@deleteRecord');
Route::get('/editRecord/{record_id}','ClinicHistoryController@editRecord');
Route::post('editRecord','ClinicHistoryController@editingRecord');


Route::get('/bloodBank/{worker_id}/{workspace_id}','BloodBankController@hospitalStore');
Route::get('/lifebankStore','BloodBankController@lifebank');

Route::get('/imergencyCall/{worker_id}/{workspace_id}','ImergencyController@imergency');
Route::post('callAmbulance','ImergencyController@callAmbulance');
Route::get('/problemReporting/{worker_id}/{workspace_id}','ProblemReporting@report');

Route::post('loginSubmit','LoginSubmitController@login');

Route::post('reportToHead','ProblemReporting@reportToHead');