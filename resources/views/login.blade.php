<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SmartClinic -Login page</title>
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
    <section class="login-page">
        <a href="home" class="back"><span>&larr;</span> GO BACK</a>
        <div class="form-container">
            <div class="logos">
                 <a class="navbar-brand" href="index.html"><img src="images/logo.png" class="logo" alt=""> <h2 class="logo-name">SmartClinic</h2></a>
            </div>
            <h1>LOGIN TO YOUR WORKSPACE</h1>
            @if($message=Session::get('success'))
                <p>{{$message}}</p>
            @endif
            <form action="loginSubmit" method="POST">
                    @csrf
                    <input type="text" name="workspace-name" placeholder="workspace name" required><br><br>
                    <input type="email" name="workspace-email" placeholder="worker email"  required><br><br>
                    <input type="password" name="workspace-pass" placeholder="worker password" required><br><br>
                    <div class="checkbox">
                        <input class="checks" type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                        <label for="rememberme">Remember Me</label>
                    </div> 
                    <button>LOGIN NOW</button><br><br>
                    <a href="signup">I don`t have the account ?</a>
            </form>
        </div>
    </section>
</body>
</html>