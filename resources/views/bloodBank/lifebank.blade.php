@foreach($worker as $profile)
@endforeach
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$profile->full_name}}`s Dashboard</title>
    <link rel="stylesheet" href="css/dashboard.css">
</head>
<body>
    <section class="dashboard">
        <section class="dashboard-left">
            <a class="logo-container" href="home"><img src="images/logo.png" class="logo" alt=""> <h2 class="logo-name">SmartClinic</h2></a>    
            <img class="profile-pic" src="images/icons/doctor.png">
            <p class="full-name">DR .{{$profile->full_name}}</p>
            <p class="department"><span>O</span> {{$profile->department}}</p>
            <div class="navbar-menu">
                <ul>
                    <li><a href="patientRecords">PATIENT RECORDS</a></li>
                    <li><a href="clinicHistory">CLINIC HISTORY</a></li> 
                    <li class="active"><a href="bloodBank">BLOOD BANK</a></li>
                    <li><a href="imergencyCall">IMERGENCY CALL</a></li>
                    <li><a href="problemReporting">PROBLEM REPORTING</a></li>
                </ul>
            </div>
        </section>
        <section class="dashboard-right">
            <section class="dashboard-right-header">
                <ul>
                    <li><a href="bloodBank">hospital blood store</a></li>
                    <li  class="active"><a href="lifebankStore">online life bank store</a></li>
                    <li><a href="droneStore">rwanda drone blood</a></li>
                    <li><a href="otherBankStore">other banks</a></li>
                </ul>
            </section>
            <section class="dashboard-body-center">
                <form class="form-search" action="GET">
                    <input class="search-blood" type="text" placeholder="search blood using group">
                    <button><img src="images/icons/search.png"></button>
                </form>
            </section>
            <section class="dashboard-body">
                <div class="banks">
                    <p>New in bank (3) </p><hr>
                    <div class="latest-in-store">
                        @foreach($bloodBank as $blood)
                            <div class="blood-group">
                                <div>
                                    <button>TAKE</button>
                                </div>
                                <div>
                                    <h2>STORE : {{$blood->banked_at}}</h2>
                                    <div>
                                        <h3>BLOOD GROUP : {{$blood->blood_group}}</h3>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <p>Other in store(6) </p><hr>
                    <div class="latest-in-store">
                        @foreach($bloodBank as $blood)
                        <div class="blood-group">
                                <div>
                                    <button>TAKE</button>
                                </div>
                                <div>
                                    <h2>STORE : {{$blood->banked_at}}</h2>
                                    <div>
                                        <h3>BLOOD GROUP : {{$blood->blood_group}}</h3>
                                    </div>
                                </div>
                        </div>
                        <div class="blood-group">
                                <div>
                                    <button>TAKE</button>
                                </div>
                                <div>
                                    <h2>STORE : {{$blood->banked_at}}</h2>
                                    <div>
                                        <h3>BLOOD GROUP : {{$blood->blood_group}}</h3>
                                    </div>
                                </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </section>
        </section>
    </section>
<script src="js/jquery-3.2.1.min.js"></script>
</body>
</html>