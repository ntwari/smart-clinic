@foreach($worker as $profile)
@endforeach
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$profile->full_name}} `s Blood Store</title>
    <link rel="stylesheet" href="http://127.0.0.1:8000/css/dashboard.css">
</head>
<body>
    <section class="dashboard">
        <section class="dashboard-left">
            <a class="logo-container" href="home"><img src="http://127.0.0.1:8000/images/logo.png" class="logo" alt="">  <h2 class="logo-name">SmartClinic</h2></a>    
            <img class="profile-pic" src="http://127.0.0.1:8000/images/icons/doctor.png">
            <p class="full-name">{{$profile->full_name}}</p>
            <p class="department"><span>O</span> {{$profile->department}}</p>
            <div class="navbar-menu">
                <ul>
                    <li><a href="http://127.0.0.1:8000/patientRecords/{{$profile->worker_id}}/{{$profile->workspace_id}}">PATIENT RECORDS</a></li>
                    <li><a href="http://127.0.0.1:8000/clinicHistory/{{$profile->worker_id}}/{{$profile->workspace_id}}">CLINIC HISTORY</a></li> 
                    <li class="active"><a href="http://127.0.0.1:8000/bloodBank/{{$profile->worker_id}}/{{$profile->workspace_id}}">BLOOD BANK</a></li>
                    <li><a href="http://127.0.0.1:8000/imergencyCall/{{$profile->worker_id}}/{{$profile->workspace_id}}">IMERGENCY CALL</a></li>
                    <li><a href="http://127.0.0.1:8000/problemReporting/{{$profile->worker_id}}/{{$profile->workspace_id}}">PROBLEM REPORTING</a></li>
                </ul>
            </div>
        </section>
        <section class="dashboard-right">
            <section class="dashboard-right-header">
                <ul>
                    <li  class="active"><a href="bloodBank">hospital blood store</a></li>
                    <li><a href="http://127.0.0.1:8000/lifebankStore">online life bank store</a></li>
                    <li><a href="http://127.0.0.1:8000/droneStore">rwanda drone blood</a></li>
                    <li><a href="http://127.0.0.1:8000/otherBankStore">other banks</a></li>
                </ul>
            </section>
            <section class="dashboard-body-center">
                <form class="form-search" action="GET">
                    <input class="search-blood" type="text" placeholder="search blood using group">
                    <button><img src="http://127.0.0.1:8000/images/icons/search.png"></button>
                </form>
            </section>
            <section class="dashboard-body">
                <div class="banks">
                    <p>New in bank (3) </p><hr>
                    <div class="latest-in-store">
                        @foreach($bloodBank as $blood)
                            <div class="blood-group">
                                <div>
                                    <button>TAKE</button>
                                </div>
                                <div>
                                    <h2>STORE : {{$blood->banked_at}}</h2>
                                    <div>
                                        <h3>BLOOD GROUP : {{$blood->blood_group}}</h3>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <p>Other in store(0) </p><hr>
                    <div class="error-container">
                        <img src="http://127.0.0.1:8000/images/icons/error.png">
                        <p>NO OTHER BLOOD IN OUR HOSPITAL BANK </p>
                    </div>
                </div>
            </section>
        </section>
    </section>
<script src="js/jquery-3.2.1.min.js"></script>
</body>
</html>