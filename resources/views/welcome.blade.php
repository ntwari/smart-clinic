<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SmartClinic - Welcome page</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/app.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">
</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
        <!-- Start Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light bg-white navbar-default bootsnav">
            <div class="container">
                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                    <a class="navbar-brand" href="index.html"><img src="images/logo.png" class="logo" alt=""> <h2 class="logo-name">SmartClinic</h2></a>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                        <li class="nav-item active"><a class="nav-link" href="#">HOME</a></li>
                        <li class="nav-item"><a class="nav-link" href="#about">ABOUT US</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">SERVICES</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">MEMORIES</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">OUR PATTERNERS</a></li>
                        <li class="nav-item"><a class="nav-link" id="linking" href="signup">CREATE WORKSPACE</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            </nav>
        <!-- End Navigation -->
    </header>
    <!-- slide show here -->
    <section class="background-section">
        <h1>SmartClinic</h1>
        <p>SmartClinic is web based app for supporting hospitals to  have good management and operations               through       online</p>
        <a class="goto-work" href="login">GOTO TO YOUR WORKSPACE</a>
    </section>
    <section class="about-us" id="about">
        <h1>ABOUT US</h1>
        <div class="about-us-all">
            <h2>SmartClinic</h2>
            <p>In Rwanda , Hospitals are using the analog ways of recording ,  payment and management  which is reading to the lost of some  information  due to damage  after  some time </p>
            <p>That is where we come as <strong>SmartClinic</strong> by using your created workspace , you can do it all for free</p>
        </div>
    </section>
    <section class="services-container">
            <h2>SERVICES</h2>
            <div class="services-all">
            <div class="services">
                    <h3>RECORD MANAGEMENT</h3>
                    <img src="../images/services.png" alt="img">
                    <p>SmartClinic   provide    the    best     room ofrecording     the      patient       and   medicine          records</p>
                </div>
                <div class="services">
                    <h3>SMART CLINIC</h3>
                    <img src="../images/services.png" alt="img">
                    <p>SmartClinic   provide    the    best     room ofrecording     the      patient       and   medicine          records</p>
                </div>
                <div class="services">
                    <h3>BLOOD BANK</h3>
                    <img src="../images/services.png" alt="img">
                    <p>SmartClinic   provide    the    best     room ofrecording     the      patient       and   medicine          records</p>
                </div>
                <div class="services">
                    <h3>COMMUNICATION IN HOSPITAL</h3>
                    <img src="../images/services.png" alt="img">
                    <p>SmartClinic   provide    the    best     room ofrecording     the      patient       and   medicine          records</p>
                </div>
                <div class="services">
                    <h3>GOOD PAYMENT</h3>
                    <img src="../images/services.png" alt="img">
                    <p>SmartClinic   provide    the    best     room ofrecording     the      patient       and   medicine          records</p>
                </div>
                <div class="services">
                    <h3>ASSETS MANAGEMENT</h3>
                    <img src="../images/services.png" alt="img">
                    <p>SmartClinic   provide    the    best     room ofrecording     the      patient       and   medicine          records</p>
                </div>
            </div>
    </section>
    <!-- <section class="testimonies">
        <h2>TESTIMONIES</h2>
    </section> -->
    <footer>
        <div class="footer-main">
            <div class="container">
		
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-widget">
                            <h4>About smartclinic</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p> 
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p> 							
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-link">
                            <h4>Quick Link</h4>
                            <ul>
                                <li><a href="#">Turi Bande</a></li>
                                <li><a href="#">Tuvugishe</a></li>
                                <li><a href="#">Injira</a></li>
                                <li><a href="#">Twisunge</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-link-contact">
                            <h4>Tuvugishe</h4>
                            <ul>
                                <li>
                                    <p><i class="fas fa-map-marker-alt"></i>Address: Rwanda Coding Academy <br>Nyabihu, Mukamira </p>
                                </li>
                                <li>
                                    <p><i class="fas fa-phone-square"></i>Phone: <a href="tel:+250 787093022">+250 787093022</a></p>
                                </li>
                                <li>
                                    <p><i class="fas fa-envelope"></i>Email: <a href="mailto:contactinfo@gmail.com">contactinfo@gmail.com</a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </footer>
    <!-- End Footer  -->

    <!-- Start copyright  -->
    <div class="footer-copyright">
        <p class="footer-company">All Rights Reserved. &copy; 2020 <a href="#">SmartClinic</a>  By :
           Ntwari Egide.</a></p>
    </div>
    <a href="#" id="back-to-top" title="Back to top" style="display: none;color: blue;font-size: 30px">&uarr;</a>

    <!-- ALL JS FILES -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.superslides.min.js"></script>
    <script src="js/custom.js"></script>
</body>

</html>