@foreach($worker as $profile)
@endforeach
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$profile->full_name}} - Problem Reporting </title>
    <link rel="stylesheet" href="http://127.0.0.1:8000/css/dashboard.css">
</head>
<body>
    <section class="dashboard">
        <section class="dashboard-left">
            <a class="logo-container" href="home"><img src="http://127.0.0.1:8000/images/logo.png" class="logo" alt="">  <h2 class="logo-name">SmartClinic</h2></a>    
            <img class="profile-pic" src="http://127.0.0.1:8000/images/icons/doctor.png">
            <p class="full-name">{{$profile->full_name}}</p>
            <p class="department"><span>O</span> {{$profile->department}}</p>
            <div class="navbar-menu">
                <ul>
                    <li><a href="http://127.0.0.1:8000/patientRecords/{{$profile->worker_id}}/{{$profile->workspace_id}}">PATIENT RECORDS</a></li>
                    <li><a href="http://127.0.0.1:8000/clinicHistory/{{$profile->worker_id}}/{{$profile->workspace_id}}">CLINIC HISTORY</a></li> 
                    <li><a href="http://127.0.0.1:8000/bloodBank/{{$profile->worker_id}}/{{$profile->workspace_id}}">BLOOD BANK</a></li>
                    <li><a href="http://127.0.0.1:8000/imergencyCall/{{$profile->worker_id}}/{{$profile->workspace_id}}">IMERGENCY CALL</a></li>
                    <li class="active"><a href="http://127.0.0.1:8000/problemReporting/{{$profile->worker_id}}/{{$profile->workspace_id}}">PROBLEM REPORTING</a></li>
                </ul>
            </div>
        </section>
        <section class="dashboard-right">
            <section class="dashboard-body imergency-body">
                <div class="report-to">
                    @foreach($head as $hoh)
                    <div class="head hoh-reporting reporters">
                        <div class="profile">
                            <img src="http://127.0.0.1:8000/images/icons/hoh.jpg" alt="">
                            <div>
                                <h2>HEAD OF HOSPITAL</h2>
                                <p>{{$hoh->full_name}}</p>
                                <p class="email">{{$hoh->email}}</p>
                            </div>
                        </div>
                        <button class="report-to-hoh">REPORTING</button>
                    </div>
                    @endforeach

                    @foreach($manager as $man)
                    <div class="reporters">
                        <div class="profile">
                            <img src="http://127.0.0.1:8000/images/icons/manager.png" alt="">
                            <div>
                                <h2>MANAGER OF HOSPITAL</h2>
                                <p>{{$man->full_name}}</p>
                                <p class="email">{{$man->email}}</p>
                            </div>
                        </div>
                        <button class="report-to-manager">REPORT</button>
                    </div>
                    @endforeach
                    
                    
                    @foreach($accountant as $acc)
                    <div class="reporters">
                        <div class="profile">
                            <img src="http://127.0.0.1:8000/images/icons/accountant.jpg" alt="">
                            <div>
                                <h2>ACCOUNT OF HOSPITAL</h2>
                                <p>{{$acc->full_name}}</p>
                                <p class="email">{{$acc->email}}</p>
                            </div>
                        </div>
                        <button class="report-to-accountant">REPORT</button>
                    </div>
                    @endforeach
                   
                </div>
                <div class="problem-reporting-container">
                    <p>Calling Head of Hospital </p> <hr>
                    <div class="contact-hoh">
                       <form action="http://127.0.0.1:8000/reportToHead" method="POST">
                            @csrf
                            <input type="hidden" name="reporter" value="{{$profile->worker_id}}">
                            <input type="hidden" name="hoh_id" value="{{$hoh->worker_id}}">
                            <textarea name="report" cols="30" rows="10" placeholder="some thing to say"></textarea><br><br>
                            <button>REPORT NOW</button>
                       </form>     
                    </div>
                    <p>Calling Manager of Hospital </p> <hr>
                    <div class="contact-manager">
                        <form action="http://127.0.0.1:8000/reportToHead" method="POST">
                            @csrf
                            <input type="hidden" name="reporter" value="{{$profile->worker_id}}">
                            <input type="hidden" name="hoh_id" value="{{$man->worker_id}}">
                            <textarea name="report" cols="30" rows="10" placeholder="some thing to say"></textarea><br><br>
                            <button>REPORT NOW</button>
                       </form> 
                    </div>


                    <p>Calling Accountant of Hospital </p> <hr>
                    <div class="contact-accountant">
                        <form action="http://127.0.0.1:8000/reportToHead" method="POST">
                            @csrf
                            <input type="hidden" name="reporter" value="{{$profile->worker_id}}">
                            <input type="hidden" name="hoh_id" value="{{$acc->worker_id}}">
                            <textarea name="report" cols="30" rows="10" placeholder="some thing to say"></textarea><br><br>
                            <button>REPORT NOW</button>
                       </form> 
                    </div>
                </div>
            </section>
        </section>
    </section>
    <script src="http://127.0.0.1:8000/js/jquery-3.2.1.min.js"></script>
<script>
$(document).ready(function(){
    $(".report-to-hoh").click(function(){
        $(".contact-hoh").slideDown(800);
        $(".contact-manager").slideUp();
        $(".contact-accountant").slideUp();
    });
    $(".report-to-manager").click(function(){
        $(".contact-manager").slideDown(800);
        $(".contact-hoh").slideUp();
        $(".contact-accountant").slideUp();
    });

    $(".report-to-accountant").click(function(){
        $(".contact-accountant").slideDown(800);
        $(".contact-manager").slideUp();
        $(".contact-hoh").slideUp();
    });
});
</script>
</body>
</html>