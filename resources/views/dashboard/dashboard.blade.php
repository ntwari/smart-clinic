@foreach($worker as $profile)
@endforeach
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$profile->full_name}}`s Dashboard</title>
    <link rel="stylesheet" href="http://127.0.0.1:8000/css/dashboard.css">
</head>
<body>
    <section class="dashboard">
        <section class="dashboard-left">
            <a class="logo-container" href="home"><img src="http://127.0.0.1:8000/images/logo.png" class="logo" alt=""> <h2 class="logo-name">SmartClinic</h2></a>    
            <img class="profile-pic" src="http://127.0.0.1:8000/images/icons/doctor.png">
            <p class="full-name">DR .{{$profile->full_name}}</p>
            <p class="department"><span>O</span> {{$profile->department}}</p>
            <div class="navbar-menu">
                <ul>
                    <li class="active"><a href="http://127.0.0.1:8000/patientRecords">PATIENT RECORDS</a></li>
                    <li><a href="http://127.0.0.1:8000/clinicHistory/{{$profile->worker_id}}/{{$profile->workspace_id}}">CLINIC HISTORY</a></li> 
                    <li><a href="http://127.0.0.1:8000/bloodBank/{{$profile->worker_id}}/{{$profile->workspace_id}}">BLOOD BANK</a></li>
                    <li><a href="http://127.0.0.1:8000/imergencyCall/{{$profile->worker_id}}/{{$profile->workspace_id}}">IMERGENCY CALL</a></li>
                    <li><a href="http://127.0.0.1:8000/problemReporting/{{$profile->worker_id}}/{{$profile->workspace_id}}">PROBLEM REPORTING</a></li>
                </ul>
            </div>
        </section>
        <section class="dashboard-right">
            <section class="dashboard-right-header">
                <form class="form-search" action="">
                    <input type="text" placeholder="search record by name">
                    <button><img src="http://127.0.0.1:8000/images/icons/search.png"></button>
                </form>
            </section>
            <section class="dashboard-body-center">
                <div class="icons">
                    <button><img src="http://127.0.0.1:8000/images/icons/open.png"></button>
                    <button><img src="http://127.0.0.1:8000/images/icons/edit.png"></button>
                    <button><img src="http://127.0.0.1:8000/images/icons/delete.png"></button>
                </div>
                <button class="add-button">Add new record</button>
            </section>
            <section class="add-new">
                <h2>ADD NEW PATIENT RECORDS</h2>   
                <form action="http://127.0.0.1:8000/addNewRecord/{{$profile->worker_id}}/{{$profile->workspace_id}}" method="POST">
                    @csrf
                    <h4>Personal : </h4><hr>
                        <input type="hidden" name="workspace_id" value="{{$profile->workspace_id}}">
                        <input type="hidden" name="date" value="{{$todayDate}}">
                        @if($todayHistory=='[]')
                            <input type="hidden" name="first" value="true">
                        @else
                            <input type="hidden" name="first" value="false">
                        @endif

                        <input type="text" placeholder="First Name : " name="firstName">
                        <input type="text" placeholder="Second Name : " name="secondName"><br><br>
                    <h4>Location : </h4><hr>
                        <input type="text" placeholder="District : " name="district">
                        <input type="text" placeholder="Sector : " name="sector"><br><br>
                        <input type="text" placeholder="Village : " name="village">
                        <input type="text" placeholder="Cell : " name="cell">
                    <h4>Medical : </h4><hr>
                        <textarea name="symptoms" id="" cols="100" rows="10" placeholder="Symptoms :"></textarea>
                        <textarea name="medicine" id="" cols="100" rows="10" placeholder="Medicine : "></textarea><br><br>
                        <input type="number" class="number" name="payed" id="" placeholder="Amount To Pay :"><br><br>
                    <button>ADD NEW</button>
                    <p class="close">X</p>
                </form>      
            </section>
            <section class="dashboard-body">
            <table>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>first name</th>
                        <th>second name</th>
                        <th>symptoms</th>
                        <th>medicine</th>
                        <th>amount to pay</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                        @if($todayHistory=='[]')
                        <div class="table-desc-command">
                            <h2>THE TABLE OF CLINIC HISTORY OF TODAY  ON {{date('Y-m-d')}} IS STILL EMPTY YOU NEED TO CREATE NEW ONE</h2>     
                            In order to : 
                            <ul>
                                <li>Adding new patient record </li>
                                <li>Read all stored document and generate pdf</li>
                                <li>Editing certain patient record</li>
                                <li>Deleting certain patient record</li>
                            </ul>                  
                        </div>
                        @else
                            <span class="table-desc">THE TABLE OF CLINIC HISTORY OF TODAY  ON {{date('Y-m-d')}}</span>
                            @foreach($todayHistory as $history)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$history->first_name}}</td>
                                <td>{{$history->second_name}}</td>
                                <td>{{$history->symptoms}}</td>
                                <td>{{$history->medicine}}</td>
                                <td>{{$history->payed}}</td>
                                <td><a href="http://127.0.0.1:8000/editRecord/{{$history->id}}" target="_blank">Edit</a></td>
                                <td><a href="http://127.0.0.1:8000/deleteRecord/{{$history->id}}" target="_blank">delete</a></td>
                            </tr>
                            @endforeach
                        @endif
                    
                </tbody>
            </table>
            </section>
        </section>
    </section>
<script src="http://127.0.0.1:8000/js/jquery-3.2.1.min.js"></script>
<script>
$(document).ready(function(){
  $(".add-button").click(function(){
    $(".add-new").slideDown(800);
  });
  $(".close").click(function(){
    $(".add-new").slideUp(800);
  });
});
</script>
</body>
</html>