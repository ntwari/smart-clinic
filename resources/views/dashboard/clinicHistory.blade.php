@foreach($worker as $profile)
@endforeach
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$profile->full_name}} `s clinic History</title>
    <link rel="stylesheet" href="http://127.0.0.1:8000/css/dashboard.css">
</head>
<body>
    <section class="dashboard">
        <section class="dashboard-left">
            <a class="logo-container" href="home"><img src="http://127.0.0.1:8000/images/logo.png" class="logo" alt="">  <h2 class="logo-name">SmartClinic</h2></a>    
            <img class="profile-pic" src="http://127.0.0.1:8000/images/icons/doctor.png">
            <p class="full-name">{{$profile->full_name}}</p>
            <p class="department"><span>O</span> {{$profile->department}}</p>
            <div class="navbar-menu">
                <ul>
                    <li><a href="http://127.0.0.1:8000/patientRecords/{{$profile->worker_id}}/{{$profile->workspace_id}}">PATIENT RECORDS</a></li>
                    <li class="active"><a href="http://127.0.0.1:8000/clinicHistory/{{$profile->worker_id}}/{{$profile->workspace_id}}">CLINIC HISTORY</a></li> 
                    <li><a href="http://127.0.0.1:8000/bloodBank/{{$profile->worker_id}}/{{$profile->workspace_id}}">BLOOD BANK</a></li>
                    <li><a href="http://127.0.0.1:8000/imergencyCall/{{$profile->worker_id}}/{{$profile->workspace_id}}">IMERGENCY CALL</a></li>
                    <li><a href="http://127.0.0.1:8000/problemReporting/{{$profile->worker_id}}/{{$profile->workspace_id}}">PROBLEM REPORTING</a></li>
                </ul>
            </div>
        </section>
        <section class="dashboard-right">
            <section class="dashboard-right-header">
                <form class="form-search" action="">
                    <input type="date" placeholder="search record by date">
                    <button><img src="http://127.0.0.1:8000/images/icons/search.png"></button>
                </form>
            </section>
            <section class="dashboard-body history">
            
                <p class="animated">Earlier records (2):</p> <hr> 
                <div class="latest-container">
                @foreach($allhistory as $history) 
                    @endforeach
                            <div class="latest">
                                <div class="button">
                                <button><a target="_blank" href="http://127.0.0.1:8000/openHistory/{{$history->hist_date}}/{{$history->workspace_id}}/{{$profile->worker_id}}" class="open">OPEN</a></button><br><br>
                                        <button><a target="_blank" href="http://127.0.0.1:8000/generatepdf/{{$history->hist_date}}/{{$history->workspace_id}}" class="generate-pdf">GENERATE PDF</a></button>
                                </div>
                                <div class="data">
                                        @if($history->hist_date == date('Y-m-d'))
                                        <p class="to-day-history">DATE: TODAY</p>
                                        @else
                                        <p>DATE: {{$history->hist_date}}</p>
                                        @endif
                                        <p>NUMBER OF RECORDS : {{$history->number_of_records}}</p>
                                </div>
                            </div>
                    
                    @foreach($latesthistory as $history) 
                            <div class="latest">
                                <div class="button">
                                <button><a target="_blank" href="http://127.0.0.1:8000/openHistory/{{$history->hist_date}}/{{$history->workspace_id}}/{{$profile->worker_id}}" class="open">OPEN</a></button><br><br>
                                        <button><a target="_blank" href="http://127.0.0.1:8000/generatepdf/{{$history->hist_date}}/{{$history->workspace_id}}" class="generate-pdf">GENERATE PDF</a></button>
                                </div>
                                <div class="data">
                                        <p>DATE: {{$history->hist_date}}</p>
                                        <p>NUMBER OF RECORDS : {{$history->number_of_records}}</p>
                                </div>
                            </div>
                    @endforeach
                </div>
                @foreach($allhistory as $history) 
                <!-- {{$counter++}} -->
                @endforeach
                <p class="animated">Other records ({{$counter}}): </p> <hr> 
                <div class="other-container">
                    @foreach($allhistory as $history) 
                    <div class="other-datas">
                                <div class="button">
                                <button><a target="_blank" href="http://127.0.0.1:8000/openHistory/{{$history->hist_date}}/{{$history->workspace_id}}/{{$profile->worker_id}}" class="open">OPEN</a></button><br><br>
                                        <button><a target="_blank" href="http://127.0.0.1:8000/generatepdf/{{$history->hist_date}}/{{$history->workspace_id}}" class="generate-pdf">GENERATE PDF</a></button>
                                </div>
                                <div class="data">
                                        @if($history->hist_date == date('Y-m-d'))
                                        <p class="to-day-history">DATE: TODAY</p>
                                        @else
                                        <p>DATE: {{$history->hist_date}}</p>
                                        @endif
                                        <p>NUMBER OF RECORDS : {{$history->number_of_records}}</p>
                                </div>
                            </div>
                    @endforeach                         
                </div>
            </section>
        </section>
    </section>
</body>
</html>