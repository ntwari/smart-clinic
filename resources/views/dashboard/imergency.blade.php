@foreach($worker as $profile)
@endforeach
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$profile->full_name}} - Imergency call</title>
    <link rel="stylesheet" href="http://127.0.0.1:8000/css/dashboard.css">
</head>
<body>
    <section class="dashboard">
        <section class="dashboard-left">
            <a class="logo-container" href="home"><img src="http://127.0.0.1:8000/images/logo.png" class="logo" alt="">  <h2 class="logo-name">SmartClinic</h2></a>    
            <img class="profile-pic" src="http://127.0.0.1:8000/images/icons/doctor.png">
            <p class="full-name">{{$profile->full_name}}</p>
            <p class="department"><span>O</span> {{$profile->department}}</p>
            <div class="navbar-menu">
                <ul>
                    <li><a href="http://127.0.0.1:8000/patientRecords/{{$profile->worker_id}}/{{$profile->workspace_id}}">PATIENT RECORDS</a></li>
                    <li><a href="http://127.0.0.1:8000/clinicHistory/{{$profile->worker_id}}/{{$profile->workspace_id}}">CLINIC HISTORY</a></li> 
                    <li><a href="http://127.0.0.1:8000/bloodBank/{{$profile->worker_id}}/{{$profile->workspace_id}}">BLOOD BANK</a></li>
                    <li class="active"><a href="http://127.0.0.1:8000/imergencyCall/{{$profile->worker_id}}/{{$profile->workspace_id}}">IMERGENCY CALL</a></li>
                    <li><a href="http://127.0.0.1:8000/problemReporting/{{$profile->worker_id}}/{{$profile->workspace_id}}">PROBLEM REPORTING</a></li>
                </ul>
            </div>
        </section>
        <section class="dashboard-right">
            <section class="dashboard-body imergency-body">
                <div class="imergency">
                    <div class="ambulance">
                        <h2>AMBULANCE</h2>
                        <p>Emergency to nearby Big Hospital</p>
                        <button class="open-ambulance">CALL</button>
                    </div>

                    <div class="imergency-doctor">
                        <h2>DOCTOR</h2>
                        <p>Emergency of calling Doctor</p>
                        <button class="open-doctor">CALL</button>
                    </div>
                </div>

                <div class="ambulance-details">
                <p>Calling Ambulance </p><hr>
                    <div class="hospital-list">
                        <form action="http://127.0.0.1:8000/callAmbulance" method="POST">
                            @csrf
                            <input type="hidden" name="workspace_id" value="{{$profile->worker_id}}">
                            @foreach($workspaces as $workspace)
                                <input type="hidden" name="called_workspace" value="{{$workspace->work_id}}">
                                <input type="radio" name="workspace-name" value="{{$workspace->work_name}}"> {{$workspace->work_name}} <br><br>
                            @endforeach
                            <textarea name="message" id="" cols="30" rows="4" placeholder="some thing to say"></textarea><br><br>
                            <button>CALL</button>
                        </form>
                    </div>
                </div>

                <div class="doctor-details">
                <p>Calling Doctor </p><hr>
                <div class="doctor-list">
                    <form action="http://127.0.0.1:8000/callAmbulance" method="POST">
                        @csrf
                        <input type="radio" name="workspace-name" value=""> DR . KAMANZI Jules <br><br>
                        <input type="radio" name="workspace-name"> DR .GISA Fred <br><br>
                        <textarea name="" id="" cols="30" rows="4" placeholder="some thing to say"></textarea><br><br>
                        <button>CALL</button>
                    </form>
                </div>
                        
                </div>
            </section>
        </section>
    </section>
    <script src="http://127.0.0.1:8000/js/jquery-3.2.1.min.js"></script>
<script>
$(document).ready(function(){
    $(".open-ambulance").click(function(){
        $(".hospital-list").slideDown(800);
        $(".doctor-list").slideUp();
    });
    $(".open-doctor").click(function(){
        $(".hospital-list").slideUp();
        $(".doctor-list").slideDown(800);
    });
});
</script>
</body>
</html>