<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SmartClinic - Signup Page</title>
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
    <section class="login-page signup-next-container">
        <a href="home" class="back"><span>&larr;</span> GO BACK</a>
        <div class="form-container signup-next">
            <div class="logos">
                 <a class="navbar-brand" href="index.html"><img src="images/logo.png" class="logo" alt=""> <h2 class="logo-name">SmartClinic</h2></a>
            </div>
            <h1>CREATE YOUR WORKSPACE</h1>
            <form action="signup_next" method="POST">
                @csrf
                    @foreach($workspace as $workspace)
                        <input type="hidden" name="workspacename" value="{{$workspace->work_name}}">
                    @endforeach
                    <h3 class="public" id="location">Location Credentials : </h3>
                        <select name="province">
                            <option>select hospital `s province</option>
                            <option value="east">  East </option>
                            <option value="west"> West </option>
                            <option value="north"> North </option>
                            <option value="south"> South </option>
                            <option value="kigali City"> Kigali City </option>
                        </select><br><br>
                        <input type="text" name="district" placeholder="District" ><br><br>
                        <input type="text" name="sector" placeholder="Sector" ><br><br>
                        <input type="text" name="village" placeholder="Village" ><br><br>
                    <h3 class="public"  id="location">Hospital Credentials : </h3>
                        <select name="hospital_type">
                            <option>Hospital Type</option>
                            <option value="government aided">  Government aided  </option>
                            <option value="private"> Private </option>
                        </select><br><br>
                        <input type="number" name="worker_num" placeholder="Number of worker ( doctors and nurses )"><br><br>
                        <select name="functionality">
                            <option>Hospital Functionality</option>
                            <option value="post de cante">  Post de cante  </option>
                            <option value="santre de cante"> Santre de cante </option>
                            <option value="hospital"> District Hospital </option>
                            <option value="santre de cante"> Other </option>
                        </select><br><br>
                            <button>NEXT</button>
            </form>
        </div>
    </section>
</body>
</html>