@foreach($worker as $profile)
@endforeach
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$profile->full_name}}`s Dashboard</title>
    <link rel="stylesheet" href="http://127.0.0.1:8000/css/dashboard.css">
</head>
<body>
    <section class="dashboard">
        <section class="dashboard-left">
            <a class="logo-container" href="home"><img src="http://127.0.0.1:8000/images/logo.png" class="logo" alt=""> <h2 class="logo-name">SmartClinic</h2></a>    
            <img class="profile-pic" src="http://127.0.0.1:8000/images/icons/doctor.png">
            <p class="full-name">DR .{{$profile->full_name}}</p>
            <p class="department"><span>O</span> {{$profile->department}}</p>
            <div class="navbar-menu">
                <ul>
                    <li><a href="http://127.0.0.1:8000/patientRecords">PATIENT RECORDS</a></li>
                    <li><a href="http://127.0.0.1:8000/clinicHistory">CLINIC HISTORY</a></li>
                    <li><a href="http://127.0.0.1:8000/bloodBank">BLOOD BANK</a></li>
                    <li><a href="http://127.0.0.1:8000/imergencyCall">IMERGENCY CALL</a></li>
                    <li><a href="http://127.0.0.1:8000/problemReporting">PROBLEM REPORTING</a></li>
                </ul>
            </div>
        </section>
        <section class="dashboard-right">
            @foreach($record as $editedRecord)
            <section class="add-new edit-record">
                <h2>EDIT THE RECORD RECORDED ON : {{$editedRecord->created_at}}</h2>   
                <form action="http://127.0.0.1:8000/editRecord" method="POST">
                    @csrf

                    <h4>Personal : </h4><hr>
                        <input type="hidden" name="record_id" value="{{$editedRecord->id}}">
                        <input type="text" placeholder="First Name : " name="firstName" value="{{$editedRecord->first_name}}">
                        <input type="text" placeholder="Second Name : " name="secondName" value="{{$editedRecord->second_name}}"><br><br>
                    <h4>Location : </h4><hr>
                        <input type="text" placeholder="District : " name="district" value="{{$editedRecord->district}}">
                        <input type="text" placeholder="Sector : " name="sector" value="{{$editedRecord->sector}}"><br><br>
                        <input type="text" placeholder="Village : " name="village" value="{{$editedRecord->village}}">
                        <input type="text" placeholder="Cell : " name="cell" value="{{$editedRecord->cell}}">
                    <h4>Medical : </h4><hr>
                        <textarea name="symptoms" id="" cols="100" rows="10" placeholder="Symptoms :">{{$editedRecord->symptoms}}</textarea>
                        <textarea name="medicine" id="" cols="100" rows="10" placeholder="Medicine : ">{{$editedRecord->medicine}}</textarea><br><br>
                        <input type="number" class="number" name="payed" id="" placeholder="Amount To Pay :" value="{{$editedRecord->payed}}"><br><br>
                    <button>EDIT RECORD</button>
                </form>      
            @endforeach
        </section>
    </section>
<script src="js/jquery-3.2.1.min.js"></script>
<script>
$(document).ready(function(){
  $(".add-button").click(function(){
    $(".add-new").slideDown(800);
  });
  $(".close").click(function(){
    $(".add-new").slideUp(800);
  });
});
</body>
</html>