<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SmartClinic - Signup Page</title>
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
    <section class="login-page">
        <a href="home" class="back"><span>&larr;</span> GO BACK</a>
        <div class="form-container">
            <div class="logos">
                 <a class="navbar-brand" href="index.html"><img src="images/logo.png" class="logo" alt=""> <h2 class="logo-name">SmartClinic</h2></a>
            </div>
            <h1>CREATE YOUR WORKSPACE</h1>
            <form action="signupSend" method="POST">
            @csrf
                    <h3 class="public">Public Credentials : </h3>
                        <p class="credentials">Enter following credentials and the confirmation address 
will be sent via the workspace and you will continue with 
creating your workspace and inviting others to join</p> 
                        <input type="text" name="workspacename" placeholder="workspace name" ><br><br>
                        <input type="email" name="workspaceemail" placeholder="Your email" ><br><br>
                        <button>NEXT</button>
            </form>
        </div>
    </section>
</body>
</html>